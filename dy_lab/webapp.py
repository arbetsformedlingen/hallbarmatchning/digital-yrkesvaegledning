import logging

import uvicorn
from fastapi import FastAPI, Form
from fastapi.templating import Jinja2Templates
from starlette.requests import Request

from jobsearch.api import JobSearchApi
from jobsearch.format_response import search_response, extract_from_ads, get_response

app = FastAPI()
templates = Jinja2Templates(directory='src/templates/')
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
jobsearch_service = JobSearchApi()
limit = 10


@app.get('/')
def search_get(request: Request):
    return get_response(request)


@app.post('/')
def search_post(request: Request,
                query: str = Form(''),
                offset: int = Form(0),
                ):
    result = jobsearch_service.search_job_ad(query, offset, limit)
    return search_response(request, query, offset, limit, extract_from_ads(result))


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
