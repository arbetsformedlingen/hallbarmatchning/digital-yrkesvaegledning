import json

import fire

import enrichment.ad
import enrichment.cv
from enrichment.api import EnrichmentApi
from jobsearch.api import JobSearchApi

jobsearch_service = JobSearchApi()
enrichment_service = EnrichmentApi()


def print_dict_as_json(dictionary):
    output = json.dumps(dictionary, indent=2, ensure_ascii=False)
    print(output)


def print_ad_summary(json_ad):
    title = json_ad.get("headline") or ""
    url = json_ad.get("webpage_url") or ""
    description = json_ad.get("description")
    if description:
        short_desc = description.get("text", "")[:50]
        short_desc = short_desc.replace('\n', ' ').replace('\r', ' ').replace('  ', ' ')
    else:
        short_desc = "no_description"
    employer = json_ad.get("employer")
    if employer:
        employer_long = "{} ({})".format(employer.get("name") or "no_name",
                                         employer.get("organization_number") or "no_org_number")
    else:
        employer_long = "no_employer"

    workplace = json_ad.get("workplace_address")
    municipality = workplace.get("municipality") if workplace else None
    if municipality:
        employer_long += " ({})".format(municipality)

    print("[id: {}] {} @ '{}', desc: {}, URL: '{}'".format(json_ad.get("id"), title, employer_long, short_desc, url))


class DyLab(object):

    @staticmethod
    def get_ad(ad_id: str):
        ad = jobsearch_service.get_job_ad(ad_id)
        print_dict_as_json(ad)

    @staticmethod
    def search_ad(query: str):
        ads = jobsearch_service.search_job_ad(query)
        print_dict_as_json(ads)

        print()
        print("=========== SUMMARY ===========")
        print()

        for ad in ads.get("hits") or []:
            print_ad_summary(ad)

    @staticmethod
    def enrich_ad(ad_id: str):
        ad = jobsearch_service.get_job_ad(ad_id)
        enrich_doc = enrichment.ad.convert_ad_for_enrichment(ad)
        enriched_data = enrichment_service.enrich(enrich_doc)
        print_dict_as_json(enriched_data)

    @staticmethod
    def enrich_cv(in_filename: str):
        with open(in_filename, "r", encoding='utf-8') as infile:
            candidate = json.load(infile)
            enrich_doc = enrichment.cv.convert_cv_for_enrichment(candidate)
            enriched_data = enrichment_service.enrich(enrich_doc)
            print_dict_as_json(enriched_data)


def main():
    fire.Fire(DyLab)


if __name__ == '__main__':
    main()
