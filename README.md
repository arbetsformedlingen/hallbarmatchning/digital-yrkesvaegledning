# Digital yrkesvägledning

Lab repository för att undersöka möjligheten till en
digital yrkesvägledningstjänst baserad på arbetsmarknadsdata,
utbildningskataloger mm.

Läs mer på [Wikin](https://gitlab.com/arbetsformedlingen/dy/digital-yrkesvaegledning/-/wikis/home).
