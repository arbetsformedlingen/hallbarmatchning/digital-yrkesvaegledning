import requests


class JobSearchApi(object):
    base_url = "https://jobsearch.api.jobtechdev.se/"

    def get_job_ad(self, ad_id: str):
        response = requests.get(headers={"accept": "application/json"},
                                url=f"{self.base_url}ad/{ad_id}")
        response.raise_for_status()

        return response.json()

    def search_job_ad(self, query: str, offset: int = 0, limit: int = 10):
        params = {'q': query} if query else {}
        params.update({'offset': str(offset), 'limit': str(limit)})

        response = requests.get(f"{self.base_url}search",
                                params=params,
                                headers={"accept": "application/json"})
        response.raise_for_status()

        return response.json()
