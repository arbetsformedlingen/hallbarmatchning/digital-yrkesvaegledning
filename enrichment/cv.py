import uuid


def convert_cv_for_enrichment(hropen_candidate):
    """
    Converts employment positions into indata for enrichment.
    :param hropen_candidate: A dict of a HR Open CandidateType.
    :return: A dict to be sent to enrichment.
    """
    out_positions = []
    for profile in hropen_candidate['profiles']:
        if profile['languageCode'] != 'sv-SE':
            break
        for employment in profile['employment']:
            for position in employment['positionHistories']:
                out_pos = {}
                if 'id' in position and 'value' in position['id']:
                    out_pos['doc_id'] = position['id']['value']
                else:
                    out_pos['doc_id'] = uuid.uuid4().__str__()
                out_pos['doc_headline'] = position['title']
                out_pos['doc_text'] = ' '.join(position['descriptions'])
                out_positions.append(out_pos)
    return {'documents_input': out_positions,
            'include_terms_info': True,
            'include_sentences': True,
            'sort_by_prediction_score': "NOT_SORTED"
            }
