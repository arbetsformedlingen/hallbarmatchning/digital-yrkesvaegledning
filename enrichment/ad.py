def convert_ad_for_enrichment(jobsearch_ad):
    out_ad = {'doc_id': jobsearch_ad['id'],
              'doc_headline': jobsearch_ad['headline'],
              'doc_text': jobsearch_ad['description']['text']}
    return {'documents_input': [out_ad],
            'include_terms_info': True,
            'include_sentences': True,
            'sort_by_prediction_score': "NOT_SORTED"
            }
