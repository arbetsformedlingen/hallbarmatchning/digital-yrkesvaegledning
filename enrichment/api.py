import requests


class EnrichmentApi(object):
    base_url = "https://jobad-enrichments-api.jobtechdev.se/"

    def enrich(self, input_doc):
        response = requests.post(headers={"accept": "application/json"},
                                 url=f"{self.base_url}enrichtextdocuments",
                                 json=input_doc)
        return response.json()
