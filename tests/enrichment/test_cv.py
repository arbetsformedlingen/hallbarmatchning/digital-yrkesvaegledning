import unittest

import enrichment.cv


class UtilsTests(unittest.TestCase):

    def test_convert_cv_for_enrichment_empty_candidate_profile(self):
        in_data = {
            'profiles': [
                {
                    'languageCode': 'sv-SE',
                    'employment': [
                        {
                            'positionHistories': [

                            ]
                        }
                    ]
                }
            ]
        }
        expected_data = {'documents_input': [],
                         'include_terms_info': True,
                         'include_sentences': True,
                         'sort_by_prediction_score': "NOT_SORTED"
                         }
        result_data = enrichment.cv.convert_cv_for_enrichment(in_data)
        self.assertEqual(expected_data, result_data)

    def test_convert_cv_for_enrichment_candidate_profile(self):
        in_data = {
            'profiles': [
                {
                    'languageCode': 'sv-SE',
                    'employment': [
                        {
                            'positionHistories': [
                                {
                                    'id': {'value': '1234'},
                                    'title': 'Snickare',
                                    'descriptions': ['Spikade spiker', 'Skruvade skruvar']
                                }
                            ]
                        }
                    ]
                }
            ]
        }
        expected_data = {'documents_input': [
            {
                'doc_id': '1234',
                'doc_headline': 'Snickare',
                'doc_text': 'Spikade spiker Skruvade skruvar'
            }
        ],
            'include_terms_info': True,
            'include_sentences': True,
            'sort_by_prediction_score': "NOT_SORTED"
        }
        result_data = enrichment.cv.convert_cv_for_enrichment(in_data)
        self.assertEqual(expected_data, result_data)


if __name__ == '__main__':
    unittest.main()
